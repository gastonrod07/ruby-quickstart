require 'rspec' 
require_relative '../model/string_count'

describe 'StringCount' do

  let(:string_count) { StringCount.new }
   
  it 'empty string should return {}' do
    characters_hash = string_count.count_character('')
    characters_hash.should include()
  end

  it '"a" string should return {"a" => 1}' do
    characters_hash = string_count.count_character('a')
    characters_hash.should include(
      "a" => 1,
    )
  end

  it '"mama" string' do
    characters_hash = string_count.count_character('mama')
    characters_hash.should include(
      "a" => 2,
      "m" => 2,
    )
  end

  it '"MamA" string' do
    characters_hash = string_count.count_character('MamA')
    characters_hash.should include(
      "a" => 1,
      "m" => 1,
      "A" => 1,
      "M" => 1,
    )
  end

end
