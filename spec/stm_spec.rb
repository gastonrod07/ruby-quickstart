require 'rspec' 
require_relative '../model/stm'

describe 'STM' do

  let(:stm) { STM.new() }

  it 'get tarjeta id' do
    tarjeta = stm.agregar_tarjeta('')
    expect(tarjeta).to eq 0
  end

  it 'cargar saldo en tarjeta' do
    saldo = 29
    expect(stm.cargar(tarjeta, saldo)).to eq saldo
  end

  it 'pagar saldo en tarjeta' do
    costo = 29
    expect(stm.cargar(tarjeta, costo)).to eq 0
  end

end