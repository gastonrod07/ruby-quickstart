require 'rspec'

describe 'String' do

  let(:string_empty) { String.new }

  it 'len empty string should return 3' do
    expect(string_empty.length()).to eq 0
  end

  let(:string_hello) { String.new('hello') }

  it 'len hello string should return 5' do
    expect(string_hello.length()).to eq 5
  end

end
