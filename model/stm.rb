class STM

  def initialize()
    @saldo_tarjetas = Hash.new(0)
    @tarjeta_id = 0

  def agregar_tarjeta(tipo)
    tarjeta = Tarjeta.new(tipo)
    @saldo_tarjetas[tarjeta] = 0
    return tarjeta

  def cargar(tarjeta, monto)
    @saldo_tarjetas[tarjeta] += monto
  end

  def pagar(tarjeta, costo, id=nil, fecha=nil)
    @saldo_tarjetas[tarjeta] -= costo
  end

end
