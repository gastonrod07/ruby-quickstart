class StringCount

  def count_character(string_to_count)
    result = Hash.new(0)
    string_to_count.each_char do |char|
      result[char] +=1
    end
    return result
  end

end
